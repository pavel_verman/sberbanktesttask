package Node;


import Entity.*;
import org.apache.ignite.*;
import org.apache.ignite.cache.query.ScanQuery;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.logger.log4j2.Log4J2Logger;

import javax.cache.Cache;
import javax.cache.processor.EntryProcessor;
import java.util.ArrayList;
import java.util.List;

public class IgniteNode {

    private static final String CACHE_USER = "USERS";
    private static final String CACHE_TRANSACTION = "TRANSACTIONS";

    private IgniteNode(){}

    public static void startNode() throws InterruptedException, IgniteException, IgniteCheckedException {

        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setPeerClassLoadingEnabled(true);
        IgniteLogger log = new Log4J2Logger("log4j2.xml");
        cfg.setGridLogger(log);

        Ignite ignite = Ignition.start(cfg);
        CacheConfiguration<Long, User> user_ccfg = new CacheConfiguration<>(CACHE_USER);
        CacheConfiguration<Long, Transaction> transaction_ccfg = new CacheConfiguration<>(CACHE_TRANSACTION);
        user_ccfg.setIndexedTypes(Long.class, Transaction.class);
        transaction_ccfg.setIndexedTypes(Long.class, User.class);
        transaction_ccfg.setSqlSchema(CACHE_TRANSACTION);
        user_ccfg.setSqlSchema(CACHE_USER);

        IgniteCache<Long, User> users = ignite.getOrCreateCache(user_ccfg);
        IgniteCache<Long, Transaction> transactions = ignite.getOrCreateCache(transaction_ccfg);

    }

    public static void populate(){
        addUser(new User(1, "Jozef", UserRegion.AP_SOUTHEAST_1));
        addUser(new User(2, "Maria", UserRegion.AP_NORTHEAST_1));
        addTransaction(new Transaction(1,1,
                System.currentTimeMillis(), System.currentTimeMillis(), TransactionType.TYPE1, null));
        addTransaction(new Transaction(2,1,
                System.currentTimeMillis(), System.currentTimeMillis(),
                TransactionType.TYPE2, TransactionStatus.STATUS1));
        addTransaction(new Transaction(3,2,
                System.currentTimeMillis(), System.currentTimeMillis(),
                TransactionType.TYPE3, TransactionStatus.STATUS1));
    }

    public static void clearAllHaches(){
        Ignite ignite = Ignition.ignite();
        IgniteCache<Long, User> users = ignite.cache(CACHE_USER);
        IgniteCache<Long, Transaction> transactions = ignite.cache(CACHE_TRANSACTION);
        users.clear();
        transactions.clear();
    }

    public static void addUser(User new_user){
        Ignite ignite = Ignition.ignite();
        IgniteCache<Long, User> users = ignite.cache(CACHE_USER);
        users.put(new_user.getId(), new_user);
    }

    public static void addTransaction(Transaction new_transaction){
        Ignite ignite = Ignition.ignite();
        IgniteCache<Long, Transaction> transactions = ignite.cache(CACHE_TRANSACTION);
        transactions.put(new_transaction.getId(), new_transaction);
    }

    public static User getUser(long user_id){
        Ignite ignite = Ignition.ignite();
        IgniteCache<Long, User> users = ignite.cache(CACHE_USER);
        return users.get(user_id);
    }

    public static Transaction getTransaction(long transaction_id){
        Ignite ignite = Ignition.ignite();
        IgniteCache<Long, Transaction> transactions = ignite.cache(CACHE_TRANSACTION);
        return transactions.get(transaction_id);
    }

    public static void changeTransactionStatus(Long transaction_id, int transaction_status){
        Ignite ignite = Ignition.ignite();
        IgniteCache<Long, Transaction> transactions = ignite.cache(CACHE_TRANSACTION);
        Transaction target_transaction = transactions.get(transaction_id);
        if ((transaction_status >= 0)&&(transaction_status < TransactionStatus.values().length)) {
            target_transaction.changeStatus(TransactionStatus.values()[transaction_status]);
            transactions.put(transaction_id, target_transaction);  // update cache
        }
    }

    public static void computeConsistentlyForRegions(EntryProcessor<Long, Transaction, Boolean> processor){
        System.out.println("Start to compute all transactions consistently for regions");
        Ignite ignite = Ignition.ignite();
        IgniteCache<Long, Transaction> transactions = ignite.cache(CACHE_TRANSACTION);
        IgniteCache<Long, Transaction> users = ignite.cache(CACHE_USER);
        for (UserRegion region: UserRegion.values()) {
            // get all users_ids for current region
            List<Long> user_ids = users.query(new ScanQuery<Long, User>(
                    (aLong, user) -> user.getRegion() == region),
                    Cache.Entry::getKey).getAll();
            // get all transactions for this users
            // so bad decision!  But what to do... Mysql Query except an error. sry :(
            //TODO make it better
            List<Long> trans_ids_for_region = new ArrayList<>();
            for (Long user_key: user_ids) {
                List<Long> trans_ids = transactions.query(new ScanQuery<Long, Transaction>(
                                (aLong, transaction) -> transaction.getUserId() == user_key),
                        Cache.Entry::getKey).getAll();
                trans_ids_for_region.addAll(trans_ids);
                //cursor.getAll().forEach((List<?> row) -> trans_ids.add((Long) row.get(0))); // this is flatten :)
            }
            computeFunction(processor, trans_ids_for_region);
        }
    }

    public static void computeForStatus1(EntryProcessor<Long, Transaction, Boolean> processor){
        System.out.println("Start to compute for all transactions with STATUS1");
        Ignite ignite = Ignition.ignite();
        IgniteCache<Long, Transaction> transactions = ignite.cache(CACHE_TRANSACTION);
        List<Long> keys = transactions.query(new ScanQuery<Long, Transaction>(
                        (k, p) -> p.getStatus() == TransactionStatus.STATUS1),
                Cache.Entry::getKey
        ).getAll();
        computeFunction(processor, keys);
    }

    public static void computeForGivenTransaction(EntryProcessor<Long, Transaction, Boolean> processor, Long transaction_id){
        System.out.println("Start to compute for transaction: " + transaction_id);
        List<Long> transaction_ids = new ArrayList<>();
        transaction_ids.add(transaction_id);
        computeFunction(processor, transaction_ids);

    }

    public static void computeFunction(EntryProcessor<Long, Transaction, Boolean> processor, List<Long> transaction_ids){

        Ignite ignite = Ignition.ignite();
        IgniteCache<Long, Transaction> transactions = ignite.cache(CACHE_TRANSACTION);
        for (Long trans_id: transaction_ids){
            // Atomicity  within a lock is guaranteed
            // it may cause a deadlock
            // it can be Asynchronous (now it's not)
            if (transactions.containsKey(trans_id)) {
                System.out.println("Old: " + transactions.get(trans_id).toString());
                transactions.invoke(trans_id, processor);
                System.out.println("New: " + transactions.get(trans_id).toString());
            }
            else{
                System.out.println("Given transaction key not exists: " + trans_id);
            }
        }
    }

    public static void printAllTransactions(){
        Ignite ignite = Ignition.ignite();
        IgniteCache<Long, Transaction> updated_transactions = ignite.cache(CACHE_TRANSACTION);
        List<Long> keys = updated_transactions.query(new ScanQuery<Long, Transaction>(
                        (k, p) -> true),
                Cache.Entry::getKey
        ).getAll();
        for (Long trans_id: keys){
            System.out.println(updated_transactions.get(trans_id).toString());
        }
    }
}
