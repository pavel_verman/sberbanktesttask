package Node;

import org.apache.ignite.IgniteCheckedException;
import org.apache.ignite.IgniteException;

public class Main {
    public static void main(String[] args){
        try {
            IgniteNode.startNode();
        } catch (InterruptedException e) {
            System.out.println("Node is interrupted");
        }
        catch (IgniteCheckedException e) {
            e.printStackTrace();
        }
        catch (IgniteException e) {
            System.out.println(e.toString());
        }
    }

}
