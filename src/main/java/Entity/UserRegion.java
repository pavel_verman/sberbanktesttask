package Entity;

public enum UserRegion {
    AP_NORTHEAST_1,
    AP_NORTHEAST_2,
    AP_SOUTH_1,
    AP_SOUTHEAST_1,
    AP_SOUTHEAST_2,
    CA_CENTRAL_1,
    CN_NORTH_1,
    EU_CENTRAL_1,
    EU_WEST_1,
    EU_WEST_2,
    }
