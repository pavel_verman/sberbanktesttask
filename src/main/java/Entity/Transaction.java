package Entity;

import org.apache.ignite.Ignition;
import org.apache.ignite.cache.affinity.AffinityKeyMapped;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

public class Transaction {
    @QuerySqlField(index = true)
    private long id;

    @AffinityKeyMapped
    @QuerySqlField(index = true)
    private long user_id;

    private long create_date;  // decided to store in Long instead of java.sql.Timestamp

    private long change_date;  // decided to store in Long instead of java.sql.Timestamp

    private TransactionType type;

    private TransactionStatus status;

    public Transaction(long id, long user_id, long create_date, long change_date,
                       TransactionType type, TransactionStatus status) {
        this.id = id;
        this.user_id = user_id;
        this.create_date = create_date;
        this.change_date = change_date;
        this.type = type;
        this.status = (status == null)?TransactionStatus.INIT:status;
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return user_id;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public long getCreateDate() {
        return create_date;
    }

    public long getChangeDate() {
        return change_date;
    }

    public TransactionType getType() {
        return type;
    }

    public void changeStatus(TransactionStatus new_status){
        Ignition.ignite().log().info(String.format("[TransactionUpdated] id: %d status: %s -> %s ", getId(), getStatus(), new_status));
        this.change_date = System.currentTimeMillis();
        this.status = new_status;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", create_date=" + create_date +
                ", change_date=" + change_date +
                ", type=" + type +
                ", status=" + status +
                '}';
    }
}
