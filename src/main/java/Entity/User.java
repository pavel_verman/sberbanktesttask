package Entity;

import org.apache.ignite.cache.query.annotations.QuerySqlField;

public class User {

    @QuerySqlField(index = true)
    private long id;

    private String name;

    @QuerySqlField(index = true, descending = true)
    private UserRegion region;

    public User(long id, String name, UserRegion region) {
        this.id = id;
        this.name = name;
        this.region = region;
    }

    public User(long id, String name, int region) {
        this.id = id;
        this.name = name;
        this.region = UserRegion.values()[region % UserRegion.values().length];
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public UserRegion getRegion() {
        return region;
    }
}
