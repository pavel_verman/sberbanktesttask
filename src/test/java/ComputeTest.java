import Entity.Transaction;
import Entity.TransactionStatus;
import Node.IgniteNode;
import org.junit.Before;
import org.junit.Test;

import javax.cache.processor.EntryProcessor;
import javax.cache.processor.EntryProcessorException;
import javax.cache.processor.MutableEntry;

public class ComputeTest extends GridTest{

    @Before
    public void prepare(){
        IgniteNode.clearAllHaches();
        IgniteNode.populate();
    }

    @Test
    public void testComputeForGivenTransaction(){
        //? lambda in invoke not working. raising CacheException: Failed to deserialize
        EntryProcessor<Long, Transaction, Boolean> proc1 = new EntryProcessor<Long, Transaction, Boolean>() {
            @Override
            public Boolean process(MutableEntry<Long, Transaction> entry, Object... args) throws EntryProcessorException {

                Transaction val = entry.getValue();
                val.changeStatus(TransactionStatus.STATUS2);
                entry.setValue(val);
                return true;
            }
        };
        EntryProcessor<Long, Transaction, Boolean> proc2 = new EntryProcessor<Long, Transaction, Boolean>() {
            @Override
            public Boolean process(MutableEntry<Long, Transaction> entry, Object... args) throws EntryProcessorException {

                Transaction val = entry.getValue();
                val.changeStatus(TransactionStatus.STATUS3);
                entry.setValue(val);
                return true;
            }
        };
        EntryProcessor<Long, Transaction, Boolean> proc3 = new EntryProcessor<Long, Transaction, Boolean>() {
            @Override
            public Boolean process(MutableEntry<Long, Transaction> entry, Object... args) throws EntryProcessorException {

                Transaction val = entry.getValue();
                val.changeStatus(TransactionStatus.STATUS1);
                entry.setValue(val);
                return true;
            }
        };
        IgniteNode.computeForStatus1(proc1);
        IgniteNode.computeForGivenTransaction(proc1, 1L);
        IgniteNode.computeForGivenTransaction(proc2, 1L);
        IgniteNode.computeConsistentlyForRegions(proc3);
        IgniteNode.computeForStatus1(proc2);
        IgniteNode.computeForGivenTransaction(proc1, 5L);
    }
}
