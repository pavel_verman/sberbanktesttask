import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.junit.BeforeClass;

public class GridTest {

    private static boolean setUpIsDone = false;

    @BeforeClass
    public static void startTestNode(){
        if (setUpIsDone){
            return;
        }
        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setClientMode(true);
        cfg.setPeerClassLoadingEnabled(true);
        Ignition.start(cfg);
        setUpIsDone = true;
    }
}
