import Node.IgniteNode;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class PopulateTest extends GridTest{

    @Before
    public void prepare(){
        IgniteNode.populate();
    }

    @Test
    public void testPopulate(){
        assertNotNull(IgniteNode.getUser(1));
        assertNotNull(IgniteNode.getUser(2));
        assertNull(IgniteNode.getUser(999));
        assertNull(IgniteNode.getUser(-1));
        assertNotNull(IgniteNode.getTransaction(1));
        assertNotNull(IgniteNode.getTransaction(2));
        assertNull(IgniteNode.getTransaction(999));
        assertNull(IgniteNode.getTransaction(0));
    }


}
